from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Trace:
	"""Trace commands group definition. 4 total commands, 2 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("trace", core, parent)

	@property
	def up(self):
		"""up commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_up'):
			from .Trace_.Up import Up
			self._up = Up(self._core, self._base)
		return self._up

	@property
	def down(self):
		"""down commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_down'):
			from .Trace_.Down import Down
			self._down = Down(self._core, self._base)
		return self._down

	def clone(self) -> 'Trace':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Trace(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
