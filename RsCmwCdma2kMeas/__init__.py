"""RsCmwCdma2kMeas instrument driver
	:version: 3.8.10.8
	:copyright: 2021 by Rohde & Schwarz GMBH & Co. KG
	:license: MIT, see LICENSE for more details.
"""

__version__ = '3.8.10.8'

# Main class
from RsCmwCdma2kMeas.RsCmwCdma2kMeas import RsCmwCdma2kMeas

# Bin data format
from RsCmwCdma2kMeas.Internal.Conversions import BinIntFormat, BinFloatFormat

# Exceptions
from RsCmwCdma2kMeas.Internal.InstrumentErrors import RsInstrException, TimeoutException, StatusException, UnexpectedResponseException, ResourceError, DriverValueError

# Callback Event Argument prototypes
from RsCmwCdma2kMeas.Internal.IoTransferEventArgs import IoTransferEventArgs

# enums
from RsCmwCdma2kMeas import enums

# repcaps
from RsCmwCdma2kMeas import repcap

# Reliability interface
from RsCmwCdma2kMeas.CustomFiles.reliability import Reliability, ReliabilityEventArgs, codes_table
