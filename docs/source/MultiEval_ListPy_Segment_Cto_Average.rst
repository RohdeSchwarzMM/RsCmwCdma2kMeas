Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CTO:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CTO:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CTO:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CTO:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cto_.Average.Average
	:members:
	:undoc-members:
	:noindex: