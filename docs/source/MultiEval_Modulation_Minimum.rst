Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: