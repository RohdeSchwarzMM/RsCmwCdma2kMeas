Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Average.Average
	:members:
	:undoc-members:
	:noindex: