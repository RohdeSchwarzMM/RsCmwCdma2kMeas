Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RccCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: