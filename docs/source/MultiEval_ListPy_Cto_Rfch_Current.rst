Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RFCH:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RFCH:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RFCH:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RFCH:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.Rfch_.Current.Current
	:members:
	:undoc-members:
	:noindex: