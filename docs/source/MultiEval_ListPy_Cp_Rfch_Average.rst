Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Rfch_.Average.Average
	:members:
	:undoc-members:
	:noindex: