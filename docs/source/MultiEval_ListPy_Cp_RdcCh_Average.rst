Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RdcCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: