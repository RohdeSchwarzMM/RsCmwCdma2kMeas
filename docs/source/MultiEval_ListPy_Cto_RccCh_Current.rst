Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RccCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: