Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Current.Current
	:members:
	:undoc-members:
	:noindex: