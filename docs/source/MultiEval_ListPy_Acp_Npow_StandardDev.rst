StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: