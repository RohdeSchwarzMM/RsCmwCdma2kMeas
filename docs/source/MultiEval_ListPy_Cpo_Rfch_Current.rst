Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Rfch_.Current.Current
	:members:
	:undoc-members:
	:noindex: