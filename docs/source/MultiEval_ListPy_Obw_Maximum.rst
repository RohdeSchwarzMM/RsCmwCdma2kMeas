Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: