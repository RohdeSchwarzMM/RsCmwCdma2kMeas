Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Current_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: