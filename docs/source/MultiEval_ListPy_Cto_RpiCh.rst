RpiCh
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RpiCh.RpiCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cto.rpiCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cto_RpiCh_State.rst
	MultiEval_ListPy_Cto_RpiCh_Current.rst
	MultiEval_ListPy_Cto_RpiCh_Average.rst
	MultiEval_ListPy_Cto_RpiCh_Maximum.rst