MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:SFACtor
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:IQLCheck

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:SFACtor
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:IQLCheck



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Scount.rst
	Configure_MultiEval_ListPy.rst
	Configure_MultiEval_Acp.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_Limit.rst