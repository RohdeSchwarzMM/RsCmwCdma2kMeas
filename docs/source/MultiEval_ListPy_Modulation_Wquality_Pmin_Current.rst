Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Wquality_.Pmin_.Current.Current
	:members:
	:undoc-members:
	:noindex: