Cpo
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo.Cpo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cpo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cpo_RpiCh.rst
	MultiEval_ListPy_Cpo_RdcCh.rst
	MultiEval_ListPy_Cpo_RccCh.rst
	MultiEval_ListPy_Cpo_ReaCh.rst
	MultiEval_ListPy_Cpo_Rfch.rst
	MultiEval_ListPy_Cpo_RsCh.rst
	MultiEval_ListPy_Cpo_Current.rst
	MultiEval_ListPy_Cpo_Average.rst
	MultiEval_ListPy_Cpo_Maximum.rst
	MultiEval_ListPy_Cpo_State.rst