Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Current.Current
	:members:
	:undoc-members:
	:noindex: