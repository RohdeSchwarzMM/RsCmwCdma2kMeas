Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpm_.Average.Average
	:members:
	:undoc-members:
	:noindex: