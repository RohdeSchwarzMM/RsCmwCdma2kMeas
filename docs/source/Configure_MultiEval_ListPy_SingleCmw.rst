SingleCmw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.SingleCmw.SingleCmw
	:members:
	:undoc-members:
	:noindex: