Acp
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp.Acp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Acp_Current.rst
	MultiEval_Trace_Acp_Extended.rst
	MultiEval_Trace_Acp_Average.rst
	MultiEval_Trace_Acp_Maximum.rst