State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	single: FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	single: CALCulate:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	CALCulate:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr_.Sequence_.Trace_.Down_.State.State
	:members:
	:undoc-members:
	:noindex: