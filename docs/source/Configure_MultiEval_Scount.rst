Scount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Scount.Scount
	:members:
	:undoc-members:
	:noindex: