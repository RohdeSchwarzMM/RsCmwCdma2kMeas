Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Average.Average
	:members:
	:undoc-members:
	:noindex: