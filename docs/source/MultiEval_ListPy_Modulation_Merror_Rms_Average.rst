Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: