Cto
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto.Cto
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cto.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cto_RpiCh.rst
	MultiEval_ListPy_Cto_RdcCh.rst
	MultiEval_ListPy_Cto_RccCh.rst
	MultiEval_ListPy_Cto_ReaCh.rst
	MultiEval_ListPy_Cto_Rfch.rst
	MultiEval_ListPy_Cto_RsCh.rst
	MultiEval_ListPy_Cto_Current.rst
	MultiEval_ListPy_Cto_Average.rst
	MultiEval_ListPy_Cto_Maximum.rst
	MultiEval_ListPy_Cto_State.rst