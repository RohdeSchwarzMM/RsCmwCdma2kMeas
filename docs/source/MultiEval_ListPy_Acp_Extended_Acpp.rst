Acpp<AcpPlus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch20
	rc = driver.multiEval.listPy.acp.extended.acpp.repcap_acpPlus_get()
	driver.multiEval.listPy.acp.extended.acpp.repcap_acpPlus_set(repcap.AcpPlus.Ch1)





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpp.Acpp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.extended.acpp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Extended_Acpp_Current.rst
	MultiEval_ListPy_Acp_Extended_Acpp_Average.rst
	MultiEval_ListPy_Acp_Extended_Acpp_Maximum.rst