PwBand
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PwBand.PwBand
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.pwBand.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_PwBand_Current.rst
	MultiEval_ListPy_Modulation_PwBand_Average.rst
	MultiEval_ListPy_Modulation_PwBand_Maximum.rst
	MultiEval_ListPy_Modulation_PwBand_Minimum.rst
	MultiEval_ListPy_Modulation_PwBand_StandardDev.rst