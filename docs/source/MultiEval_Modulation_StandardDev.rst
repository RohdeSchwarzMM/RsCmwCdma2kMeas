StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: