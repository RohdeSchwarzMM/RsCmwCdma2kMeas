State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RpiCh_.State.State
	:members:
	:undoc-members:
	:noindex: