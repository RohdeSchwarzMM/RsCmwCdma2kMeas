Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RCCCh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RccCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: