Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Perror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: