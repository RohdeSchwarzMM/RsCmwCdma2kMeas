Up
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP
	single: FETCh:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP
	FETCh:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP



.. autoclass:: RsCmwCdma2kMeas.Implementations.RpInterval_.Sequence_.Trace_.Up.Up
	:members:
	:undoc-members:
	:noindex: