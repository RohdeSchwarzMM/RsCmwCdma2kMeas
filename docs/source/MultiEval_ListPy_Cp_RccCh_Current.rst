Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RccCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: