Cpo
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cpo.Cpo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.cpo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Cpo_Current.rst
	MultiEval_ListPy_Segment_Cpo_Average.rst
	MultiEval_ListPy_Segment_Cpo_Maximum.rst
	MultiEval_ListPy_Segment_Cpo_State.rst