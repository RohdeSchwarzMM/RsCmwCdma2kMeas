Cp
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp.Cp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_RpiCh.rst
	MultiEval_ListPy_Cp_RdcCh.rst
	MultiEval_ListPy_Cp_RccCh.rst
	MultiEval_ListPy_Cp_ReaCh.rst
	MultiEval_ListPy_Cp_Rfch.rst
	MultiEval_ListPy_Cp_RsCh.rst
	MultiEval_ListPy_Cp_Current.rst
	MultiEval_ListPy_Cp_Average.rst
	MultiEval_ListPy_Cp_Maximum.rst
	MultiEval_ListPy_Cp_Minimum.rst
	MultiEval_ListPy_Cp_State.rst