Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Average.Average
	:members:
	:undoc-members:
	:noindex: