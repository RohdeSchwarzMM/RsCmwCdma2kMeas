Cp
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cp.Cp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.cp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Cp_Current.rst
	MultiEval_ListPy_Segment_Cp_Average.rst
	MultiEval_ListPy_Segment_Cp_Maximum.rst
	MultiEval_ListPy_Segment_Cp_Minimum.rst
	MultiEval_ListPy_Segment_Cp_State.rst