Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:RELative



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Current_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: