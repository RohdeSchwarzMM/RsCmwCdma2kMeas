Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:ABSolute



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Maximum_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: