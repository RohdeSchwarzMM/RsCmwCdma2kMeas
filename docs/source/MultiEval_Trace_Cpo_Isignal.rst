Isignal
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Isignal.Isignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cpo.isignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cpo_Isignal_Current.rst
	MultiEval_Trace_Cpo_Isignal_Average.rst
	MultiEval_Trace_Cpo_Isignal_Maximum.rst
	MultiEval_Trace_Cpo_Isignal_State.rst