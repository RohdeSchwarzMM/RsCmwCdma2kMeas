Zoet
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RsCh_.Zoet.Zoet
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cto.rsCh.zoet.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cto_RsCh_Zoet_State.rst
	MultiEval_ListPy_Cto_RsCh_Zoet_Current.rst
	MultiEval_ListPy_Cto_RsCh_Zoet_Average.rst
	MultiEval_ListPy_Cto_RsCh_Zoet_Maximum.rst