Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RccCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: