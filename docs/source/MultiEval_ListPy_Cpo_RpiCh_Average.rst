Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RpiCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: