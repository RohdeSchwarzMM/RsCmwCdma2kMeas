Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex: