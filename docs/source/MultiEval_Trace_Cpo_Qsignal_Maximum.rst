Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Qsignal_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: