Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: