Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Average.Average
	:members:
	:undoc-members:
	:noindex: