Rfch
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Rfch.Rfch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cpo.rfch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cpo_Rfch_State.rst
	MultiEval_ListPy_Cpo_Rfch_Current.rst
	MultiEval_ListPy_Cpo_Rfch_Average.rst
	MultiEval_ListPy_Cpo_Rfch_Maximum.rst