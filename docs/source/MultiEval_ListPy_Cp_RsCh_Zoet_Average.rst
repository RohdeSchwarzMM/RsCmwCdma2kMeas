Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Zoet_.Average.Average
	:members:
	:undoc-members:
	:noindex: