Acp
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp.Acp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Otolerance.rst
	MultiEval_ListPy_Acp_StCount.rst
	MultiEval_ListPy_Acp_Acpm.rst
	MultiEval_ListPy_Acp_Extended.rst
	MultiEval_ListPy_Acp_Acpp.rst
	MultiEval_ListPy_Acp_Npow.rst
	MultiEval_ListPy_Acp_Wpow.rst
	MultiEval_ListPy_Acp_Current.rst
	MultiEval_ListPy_Acp_Average.rst
	MultiEval_ListPy_Acp_Maximum.rst
	MultiEval_ListPy_Acp_Minimum.rst
	MultiEval_ListPy_Acp_StandardDev.rst