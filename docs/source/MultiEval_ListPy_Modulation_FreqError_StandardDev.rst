StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.FreqError_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: