State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Isignal_.State.State
	:members:
	:undoc-members:
	:noindex: