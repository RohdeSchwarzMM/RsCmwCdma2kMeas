State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	single: FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	single: CALCulate:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	CALCulate:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr_.Sequence_.Trace_.Up_.State.State
	:members:
	:undoc-members:
	:noindex: