Cde
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde.Cde
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cde.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cde_Isignal.rst
	MultiEval_Trace_Cde_Qsignal.rst