Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RCCCh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RccCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: