Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RccCh_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: