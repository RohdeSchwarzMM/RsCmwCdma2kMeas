Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: