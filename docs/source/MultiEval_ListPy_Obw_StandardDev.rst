StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: