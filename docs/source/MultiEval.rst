MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CDMA:MEASurement<Instance>:MEValuation
	single: ABORt:CDMA:MEASurement<Instance>:MEValuation
	single: STOP:CDMA:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:CDMA:MEASurement<Instance>:MEValuation
	ABORt:CDMA:MEASurement<Instance>:MEValuation
	STOP:CDMA:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_State.rst
	MultiEval_Trace.rst
	MultiEval_Modulation.rst
	MultiEval_Acp.rst
	MultiEval_Obw.rst
	MultiEval_ListPy.rst