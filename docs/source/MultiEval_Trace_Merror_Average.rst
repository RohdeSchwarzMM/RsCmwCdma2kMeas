Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Merror_.Average.Average
	:members:
	:undoc-members:
	:noindex: