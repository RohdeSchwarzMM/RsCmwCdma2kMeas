Cpo
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo.Cpo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cpo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cpo_Isignal.rst
	MultiEval_Trace_Cpo_Qsignal.rst