Extended
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Acp_.Extended.Extended
	:members:
	:undoc-members:
	:noindex: