RpInterval
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.RpInterval.RpInterval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rpInterval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RpInterval_Sequence.rst