Qsignal
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Qsignal.Qsignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cpo.qsignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cpo_Qsignal_Current.rst
	MultiEval_Trace_Cpo_Qsignal_Average.rst
	MultiEval_Trace_Cpo_Qsignal_Maximum.rst
	MultiEval_Trace_Cpo_Qsignal_State.rst