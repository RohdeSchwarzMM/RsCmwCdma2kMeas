Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZTEF:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZTEF:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZTEF:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZTEF:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Ztef_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: