PnBand
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PnBand.PnBand
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.pnBand.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_PnBand_Current.rst
	MultiEval_ListPy_Modulation_PnBand_Average.rst
	MultiEval_ListPy_Modulation_PnBand_Maximum.rst
	MultiEval_ListPy_Modulation_PnBand_Minimum.rst
	MultiEval_ListPy_Modulation_PnBand_StandardDev.rst