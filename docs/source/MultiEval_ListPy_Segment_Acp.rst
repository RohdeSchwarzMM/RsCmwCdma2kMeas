Acp
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Acp.Acp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Acp_Current.rst
	MultiEval_ListPy_Segment_Acp_Extended.rst
	MultiEval_ListPy_Segment_Acp_Average.rst
	MultiEval_ListPy_Segment_Acp_Maximum.rst
	MultiEval_ListPy_Segment_Acp_Minimum.rst
	MultiEval_ListPy_Segment_Acp_StandardDev.rst