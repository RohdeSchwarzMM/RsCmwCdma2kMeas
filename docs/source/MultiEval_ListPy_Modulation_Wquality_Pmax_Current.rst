Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMAX:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMAX:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMAX:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMAX:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Wquality_.Pmax_.Current.Current
	:members:
	:undoc-members:
	:noindex: