Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Acp_.Average.Average
	:members:
	:undoc-members:
	:noindex: