Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Average.Average
	:members:
	:undoc-members:
	:noindex: