Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: