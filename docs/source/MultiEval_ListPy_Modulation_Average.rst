Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: