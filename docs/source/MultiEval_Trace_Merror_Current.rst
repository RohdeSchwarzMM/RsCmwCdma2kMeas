Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Merror_.Current.Current
	:members:
	:undoc-members:
	:noindex: