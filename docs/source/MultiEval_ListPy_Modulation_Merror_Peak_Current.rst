Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: