Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PwBand_.Average.Average
	:members:
	:undoc-members:
	:noindex: