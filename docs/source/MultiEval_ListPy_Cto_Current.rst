Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.Current.Current
	:members:
	:undoc-members:
	:noindex: