Isignal
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cto_.Isignal.Isignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cto.isignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cto_Isignal_Current.rst
	MultiEval_Trace_Cto_Isignal_Average.rst
	MultiEval_Trace_Cto_Isignal_Maximum.rst
	MultiEval_Trace_Cto_Isignal_State.rst