Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Rfch_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: