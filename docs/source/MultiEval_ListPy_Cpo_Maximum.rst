Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: