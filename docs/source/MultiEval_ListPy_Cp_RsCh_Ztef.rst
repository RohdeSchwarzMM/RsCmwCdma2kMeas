Ztef
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Ztef.Ztef
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.rsCh.ztef.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_RsCh_Ztef_State.rst
	MultiEval_ListPy_Cp_RsCh_Ztef_Current.rst
	MultiEval_ListPy_Cp_RsCh_Ztef_Average.rst
	MultiEval_ListPy_Cp_RsCh_Ztef_Maximum.rst
	MultiEval_ListPy_Cp_RsCh_Ztef_Minimum.rst