Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: