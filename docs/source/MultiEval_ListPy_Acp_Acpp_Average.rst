Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpp_.Average.Average
	:members:
	:undoc-members:
	:noindex: