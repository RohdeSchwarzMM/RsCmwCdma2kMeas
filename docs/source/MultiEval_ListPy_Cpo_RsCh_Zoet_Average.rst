Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RsCh_.Zoet_.Average.Average
	:members:
	:undoc-members:
	:noindex: