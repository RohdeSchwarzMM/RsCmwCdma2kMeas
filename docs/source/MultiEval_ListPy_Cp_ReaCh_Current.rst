Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.ReaCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: