Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.ReaCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: