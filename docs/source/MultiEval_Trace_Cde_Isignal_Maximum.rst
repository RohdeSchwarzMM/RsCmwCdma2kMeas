Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: