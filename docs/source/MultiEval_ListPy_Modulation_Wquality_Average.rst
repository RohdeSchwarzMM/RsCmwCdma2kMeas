Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Wquality_.Average.Average
	:members:
	:undoc-members:
	:noindex: