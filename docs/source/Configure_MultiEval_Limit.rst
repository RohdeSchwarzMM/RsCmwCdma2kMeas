Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:POWer
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:MERRor
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:PERRor
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CFERror
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:TTERror
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:WFQuality
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:OBW
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CDP
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CDE
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CP
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CPO
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CTO

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:POWer
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:MERRor
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:PERRor
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CFERror
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:TTERror
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:WFQuality
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:OBW
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CDP
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CDE
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CP
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CPO
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:CTO



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Acp.rst