Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: