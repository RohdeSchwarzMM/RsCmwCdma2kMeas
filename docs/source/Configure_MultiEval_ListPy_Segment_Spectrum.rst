Spectrum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SPECtrum

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SPECtrum



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Spectrum.Spectrum
	:members:
	:undoc-members:
	:noindex: