Rms
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms.Rms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.merror.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Merror_Rms_Current.rst
	MultiEval_ListPy_Modulation_Merror_Rms_Average.rst
	MultiEval_ListPy_Modulation_Merror_Rms_Maximum.rst
	MultiEval_ListPy_Modulation_Merror_Rms_StandardDev.rst