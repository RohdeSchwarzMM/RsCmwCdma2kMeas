Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RpiCh_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: