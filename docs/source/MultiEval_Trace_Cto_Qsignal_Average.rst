Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cto_.Qsignal_.Average.Average
	:members:
	:undoc-members:
	:noindex: