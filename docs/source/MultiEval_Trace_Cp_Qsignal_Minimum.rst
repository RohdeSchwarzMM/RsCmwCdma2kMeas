Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Qsignal_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: