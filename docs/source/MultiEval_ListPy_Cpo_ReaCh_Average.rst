Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.ReaCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: