Terror
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror.Terror
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.terror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Terror_Current.rst
	MultiEval_ListPy_Modulation_Terror_Average.rst
	MultiEval_ListPy_Modulation_Terror_Maximum.rst
	MultiEval_ListPy_Modulation_Terror_StandardDev.rst