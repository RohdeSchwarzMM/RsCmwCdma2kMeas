State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.State.State
	:members:
	:undoc-members:
	:noindex: