Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Qsignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: