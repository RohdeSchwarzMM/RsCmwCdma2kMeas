RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:CHANnel
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:BCLass
	single: CONFigure:CDMA:MEASurement<Instance>:RFSettings:FOFFset

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:CHANnel
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:BCLass
	CONFigure:CDMA:MEASurement<Instance>:RFSettings:FOFFset



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: