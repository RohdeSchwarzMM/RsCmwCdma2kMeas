RpInterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:RPINterval:TIME
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:RPINterval

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:OLTR:RPINterval:TIME
	CONFigure:CDMA:MEASurement<Instance>:OLTR:RPINterval



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.Oltr_.RpInterval.RpInterval
	:members:
	:undoc-members:
	:noindex: