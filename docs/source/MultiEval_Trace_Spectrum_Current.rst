Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Spectrum_.Current.Current
	:members:
	:undoc-members:
	:noindex: