Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.ReaCh_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: