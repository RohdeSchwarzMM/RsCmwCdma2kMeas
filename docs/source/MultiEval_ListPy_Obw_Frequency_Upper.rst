Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:UPPer

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:UPPer



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Upper.Upper
	:members:
	:undoc-members:
	:noindex: