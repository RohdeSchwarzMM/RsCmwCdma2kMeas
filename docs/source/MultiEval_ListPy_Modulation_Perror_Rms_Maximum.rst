Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: