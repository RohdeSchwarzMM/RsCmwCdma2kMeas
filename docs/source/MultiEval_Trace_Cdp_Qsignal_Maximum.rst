Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: