StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Wpow_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: