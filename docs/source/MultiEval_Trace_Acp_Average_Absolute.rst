Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Average_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: