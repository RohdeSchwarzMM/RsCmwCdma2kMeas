Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RdcCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: