Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Merror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: