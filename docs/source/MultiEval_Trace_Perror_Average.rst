Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Perror_.Average.Average
	:members:
	:undoc-members:
	:noindex: