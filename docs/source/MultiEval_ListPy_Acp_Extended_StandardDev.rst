StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: