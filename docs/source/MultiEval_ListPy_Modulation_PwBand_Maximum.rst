Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PwBand_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: