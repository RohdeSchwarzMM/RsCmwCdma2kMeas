StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: