Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Current.Current
	:members:
	:undoc-members:
	:noindex: