Ginterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:GINTerval:TIME
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:GINTerval

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:OLTR:GINTerval:TIME
	CONFigure:CDMA:MEASurement<Instance>:OLTR:GINTerval



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.Oltr_.Ginterval.Ginterval
	:members:
	:undoc-members:
	:noindex: