Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:ALL
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:ACP
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:OBW
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CDP
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CDE
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:POWer
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:MODQuality
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CP
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CPO
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CTO
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:IQ

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:ALL
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:ACP
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:OBW
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CDP
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CDE
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:POWer
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:MODQuality
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CP
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CPO
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:CTO
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:RESult:IQ



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Result.Result
	:members:
	:undoc-members:
	:noindex: