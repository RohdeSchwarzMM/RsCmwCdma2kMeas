Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:OBW:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Obw_.Current.Current
	:members:
	:undoc-members:
	:noindex: