Sreliability
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SRELiability
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SRELiability
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Sreliability.Sreliability
	:members:
	:undoc-members:
	:noindex: