Wpow
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Wpow.Wpow
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.wpow.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Wpow_Current.rst
	MultiEval_ListPy_Acp_Wpow_Average.rst
	MultiEval_ListPy_Acp_Wpow_Maximum.rst
	MultiEval_ListPy_Acp_Wpow_Minimum.rst
	MultiEval_ListPy_Acp_Wpow_StandardDev.rst