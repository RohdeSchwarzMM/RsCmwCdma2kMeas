Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Maximum_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: