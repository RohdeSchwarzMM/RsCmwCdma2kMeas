RdcCh
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RdcCh.RdcCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.rdcCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_RdcCh_State.rst
	MultiEval_ListPy_Cp_RdcCh_Current.rst
	MultiEval_ListPy_Cp_RdcCh_Average.rst
	MultiEval_ListPy_Cp_RdcCh_Maximum.rst
	MultiEval_ListPy_Cp_RdcCh_Minimum.rst