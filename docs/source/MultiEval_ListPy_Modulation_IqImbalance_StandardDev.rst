StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: