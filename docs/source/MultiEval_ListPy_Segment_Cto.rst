Cto
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cto.Cto
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.cto.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Cto_Current.rst
	MultiEval_ListPy_Segment_Cto_Average.rst
	MultiEval_ListPy_Segment_Cto_Maximum.rst
	MultiEval_ListPy_Segment_Cto_State.rst