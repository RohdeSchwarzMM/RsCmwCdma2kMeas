Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RdcCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: