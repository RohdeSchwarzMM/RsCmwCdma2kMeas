Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PwBand_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: