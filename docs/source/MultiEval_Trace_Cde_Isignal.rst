Isignal
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal.Isignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cde.isignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cde_Isignal_Current.rst
	MultiEval_Trace_Cde_Isignal_Average.rst
	MultiEval_Trace_Cde_Isignal_Maximum.rst
	MultiEval_Trace_Cde_Isignal_State.rst
	MultiEval_Trace_Cde_Isignal_Limit.rst