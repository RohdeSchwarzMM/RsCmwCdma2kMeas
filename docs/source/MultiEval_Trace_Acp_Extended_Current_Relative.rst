Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Current_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: