Trace
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr_.Sequence_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.sequence.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_Sequence_Trace_Up.rst
	Oltr_Sequence_Trace_Down.rst