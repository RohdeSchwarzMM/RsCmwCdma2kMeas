Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RdcCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: