Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Acp_.Current.Current
	:members:
	:undoc-members:
	:noindex: