Acpm<AcpMinus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch20
	rc = driver.multiEval.listPy.acp.extended.acpm.repcap_acpMinus_get()
	driver.multiEval.listPy.acp.extended.acpm.repcap_acpMinus_set(repcap.AcpMinus.Ch1)





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm.Acpm
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.extended.acpm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Extended_Acpm_Current.rst
	MultiEval_ListPy_Acp_Extended_Acpm_Average.rst
	MultiEval_ListPy_Acp_Extended_Acpm_Maximum.rst