Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Average.Average
	:members:
	:undoc-members:
	:noindex: