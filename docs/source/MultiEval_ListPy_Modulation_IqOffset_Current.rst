Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqOffset_.Current.Current
	:members:
	:undoc-members:
	:noindex: