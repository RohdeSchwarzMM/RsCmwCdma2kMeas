Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:LIMit

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:LIMit



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Limit.Limit
	:members:
	:undoc-members:
	:noindex: