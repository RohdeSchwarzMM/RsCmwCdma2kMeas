MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:EOFFset

.. code-block:: python

	TRIGger:CDMA:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:DELay
	TRIGger:CDMA:MEASurement<Instance>:MEValuation:EOFFset



.. autoclass:: RsCmwCdma2kMeas.Implementations.Trigger_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval_Catalog.rst
	Trigger_MultiEval_ListPy.rst