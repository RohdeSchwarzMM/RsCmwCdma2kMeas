Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RsCh_.Ztef_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: