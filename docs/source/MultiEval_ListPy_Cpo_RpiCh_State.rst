State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RPICh:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RpiCh_.State.State
	:members:
	:undoc-members:
	:noindex: