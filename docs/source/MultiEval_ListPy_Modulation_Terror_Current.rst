Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.Current.Current
	:members:
	:undoc-members:
	:noindex: