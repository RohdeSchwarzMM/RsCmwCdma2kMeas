Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: