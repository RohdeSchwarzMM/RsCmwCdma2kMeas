SingleCmw
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.SingleCmw.SingleCmw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.singleCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_SingleCmw_Connector.rst