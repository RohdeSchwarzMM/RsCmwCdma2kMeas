IqImbalance
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance.IqImbalance
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.iqImbalance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_IqImbalance_Current.rst
	MultiEval_ListPy_Modulation_IqImbalance_Average.rst
	MultiEval_ListPy_Modulation_IqImbalance_Maximum.rst
	MultiEval_ListPy_Modulation_IqImbalance_StandardDev.rst