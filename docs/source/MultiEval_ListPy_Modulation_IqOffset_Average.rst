Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqOffset_.Average.Average
	:members:
	:undoc-members:
	:noindex: