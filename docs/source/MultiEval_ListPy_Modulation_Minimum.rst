Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: