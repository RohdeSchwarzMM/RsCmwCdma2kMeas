ListPy
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Sreliability.rst
	MultiEval_ListPy_Modulation.rst
	MultiEval_ListPy_Segment.rst
	MultiEval_ListPy_Acp.rst
	MultiEval_ListPy_Obw.rst
	MultiEval_ListPy_Cp.rst
	MultiEval_ListPy_Cpo.rst
	MultiEval_ListPy_Cto.rst