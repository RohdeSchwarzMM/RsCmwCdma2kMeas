Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Wpow_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: