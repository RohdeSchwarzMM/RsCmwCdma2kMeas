Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CDMA:MEASurement<Instance>:SCENario:SALone
	single: ROUTe:CDMA:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:CDMA:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:CDMA:MEASurement<Instance>:SCENario:SALone
	ROUTe:CDMA:MEASurement<Instance>:SCENario:CSPath
	ROUTe:CDMA:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwCdma2kMeas.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Catalog.rst