Obw
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw.Obw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Obw_Frequency.rst
	MultiEval_ListPy_Obw_Current.rst
	MultiEval_ListPy_Obw_Average.rst
	MultiEval_ListPy_Obw_Maximum.rst
	MultiEval_ListPy_Obw_StandardDev.rst