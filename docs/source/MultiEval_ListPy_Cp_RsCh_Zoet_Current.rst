Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Zoet_.Current.Current
	:members:
	:undoc-members:
	:noindex: