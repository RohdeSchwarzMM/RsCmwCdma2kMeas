Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Average_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: