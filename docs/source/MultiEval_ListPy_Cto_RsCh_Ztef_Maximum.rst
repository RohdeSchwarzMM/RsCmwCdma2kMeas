Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RsCh_.Ztef_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: