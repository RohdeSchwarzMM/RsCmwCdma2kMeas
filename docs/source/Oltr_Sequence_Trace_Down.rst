Down
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN
	single: FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN
	FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN



.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr_.Sequence_.Trace_.Down.Down
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.sequence.trace.down.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_Sequence_Trace_Down_State.rst