Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Perror_.Current.Current
	:members:
	:undoc-members:
	:noindex: