Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RpiCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: