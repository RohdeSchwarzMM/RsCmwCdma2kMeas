RccCh
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RccCh.RccCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.rccCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_RccCh_State.rst
	MultiEval_ListPy_Cp_RccCh_Current.rst
	MultiEval_ListPy_Cp_RccCh_Average.rst
	MultiEval_ListPy_Cp_RccCh_Maximum.rst
	MultiEval_ListPy_Cp_RccCh_Minimum.rst