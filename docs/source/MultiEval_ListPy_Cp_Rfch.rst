Rfch
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Rfch.Rfch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.rfch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Rfch_State.rst
	MultiEval_ListPy_Cp_Rfch_Current.rst
	MultiEval_ListPy_Cp_Rfch_Average.rst
	MultiEval_ListPy_Cp_Rfch_Maximum.rst
	MultiEval_ListPy_Cp_Rfch_Minimum.rst