Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Isignal_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: