Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: