Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Average.Average
	:members:
	:undoc-members:
	:noindex: