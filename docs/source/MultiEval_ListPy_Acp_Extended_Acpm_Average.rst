Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm_.Average.Average
	:members:
	:undoc-members:
	:noindex: