Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cpo_.Current.Current
	:members:
	:undoc-members:
	:noindex: