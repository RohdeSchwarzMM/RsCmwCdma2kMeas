Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:REACh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.ReaCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: