Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: