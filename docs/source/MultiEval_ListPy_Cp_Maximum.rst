Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: