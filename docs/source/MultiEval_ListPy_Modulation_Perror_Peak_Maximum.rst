Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Peak_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: