Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RDCCh:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RdcCh_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: