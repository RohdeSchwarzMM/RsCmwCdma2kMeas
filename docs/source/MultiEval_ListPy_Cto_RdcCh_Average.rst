Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RdcCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: