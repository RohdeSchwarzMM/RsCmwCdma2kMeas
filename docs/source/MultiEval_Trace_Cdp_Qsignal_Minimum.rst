Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: