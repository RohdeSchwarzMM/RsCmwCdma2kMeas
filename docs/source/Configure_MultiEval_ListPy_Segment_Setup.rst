Setup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Setup.Setup
	:members:
	:undoc-members:
	:noindex: