State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Isignal_.State.State
	:members:
	:undoc-members:
	:noindex: