Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:QSIGnal:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cto_.Qsignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: