Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:LIMit:ILOWer

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:OLTR:LIMit:ILOWer



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.Oltr_.Limit.Limit
	:members:
	:undoc-members:
	:noindex: