Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RCCCh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RccCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: