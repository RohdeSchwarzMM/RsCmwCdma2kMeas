Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Isignal_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: