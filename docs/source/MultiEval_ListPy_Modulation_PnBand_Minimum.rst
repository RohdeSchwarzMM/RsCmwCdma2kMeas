Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.PnBand_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: