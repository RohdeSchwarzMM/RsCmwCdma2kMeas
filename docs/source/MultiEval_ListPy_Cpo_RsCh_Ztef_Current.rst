Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZTEF:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RsCh_.Ztef_.Current.Current
	:members:
	:undoc-members:
	:noindex: