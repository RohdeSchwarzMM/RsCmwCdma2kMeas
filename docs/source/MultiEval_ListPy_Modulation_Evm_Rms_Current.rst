Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: