Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: