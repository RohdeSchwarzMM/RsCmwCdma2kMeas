Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RPICh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RpiCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: