Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cp_.Average.Average
	:members:
	:undoc-members:
	:noindex: