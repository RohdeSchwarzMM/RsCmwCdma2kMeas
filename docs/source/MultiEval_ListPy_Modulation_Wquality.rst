Wquality
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Wquality.Wquality
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.wquality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Wquality_Current.rst
	MultiEval_ListPy_Modulation_Wquality_Pmax.rst
	MultiEval_ListPy_Modulation_Wquality_Pmin.rst
	MultiEval_ListPy_Modulation_Wquality_Average.rst
	MultiEval_ListPy_Modulation_Wquality_Maximum.rst
	MultiEval_ListPy_Modulation_Wquality_StandardDev.rst