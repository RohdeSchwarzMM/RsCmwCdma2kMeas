Up
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP
	single: FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP
	FETCh:CDMA:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP



.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr_.Sequence_.Trace_.Up.Up
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.sequence.trace.up.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_Sequence_Trace_Up_State.rst