StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: