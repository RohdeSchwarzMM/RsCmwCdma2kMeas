Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Rfch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: