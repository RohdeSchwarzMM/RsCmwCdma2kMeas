Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.ReaCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: