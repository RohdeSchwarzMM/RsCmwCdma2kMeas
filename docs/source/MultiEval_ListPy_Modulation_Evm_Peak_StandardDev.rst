StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: