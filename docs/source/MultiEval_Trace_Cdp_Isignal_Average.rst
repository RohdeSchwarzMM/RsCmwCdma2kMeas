Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Average.Average
	:members:
	:undoc-members:
	:noindex: