Down
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN
	single: FETCh:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN
	FETCh:CDMA:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN



.. autoclass:: RsCmwCdma2kMeas.Implementations.RpInterval_.Sequence_.Trace_.Down.Down
	:members:
	:undoc-members:
	:noindex: