Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Zoet_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: