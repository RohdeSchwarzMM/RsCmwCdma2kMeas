Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:ISIGnal:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Isignal_.Average.Average
	:members:
	:undoc-members:
	:noindex: