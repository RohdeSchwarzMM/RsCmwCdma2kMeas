Modulation
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Current.rst
	MultiEval_Modulation_Average.rst
	MultiEval_Modulation_Maximum.rst
	MultiEval_Modulation_Minimum.rst
	MultiEval_Modulation_StandardDev.rst