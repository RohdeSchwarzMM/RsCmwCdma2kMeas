Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: