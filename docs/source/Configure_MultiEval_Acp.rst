Acp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:FOFFsets
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:RBW

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:FOFFsets
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:ACP:RBW



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Acp.Acp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Acp_Extended.rst