Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpm_.Current.Current
	:members:
	:undoc-members:
	:noindex: