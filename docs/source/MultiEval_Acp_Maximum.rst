Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Acp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: