Acp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:ACP:RELative
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:ACP:ABSolute

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:ACP:RELative
	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIMit:ACP:ABSolute



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.Limit_.Acp.Acp
	:members:
	:undoc-members:
	:noindex: