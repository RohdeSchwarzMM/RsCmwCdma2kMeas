Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: