Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Average.Average
	:members:
	:undoc-members:
	:noindex: