RdcCh
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RdcCh.RdcCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cpo.rdcCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cpo_RdcCh_State.rst
	MultiEval_ListPy_Cpo_RdcCh_Current.rst
	MultiEval_ListPy_Cpo_RdcCh_Average.rst
	MultiEval_ListPy_Cpo_RdcCh_Maximum.rst