Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:REACh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.ReaCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: