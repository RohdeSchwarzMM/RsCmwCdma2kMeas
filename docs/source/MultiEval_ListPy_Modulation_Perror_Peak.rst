Peak
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Peak.Peak
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.perror.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Perror_Peak_Current.rst
	MultiEval_ListPy_Modulation_Perror_Peak_Average.rst
	MultiEval_ListPy_Modulation_Perror_Peak_Maximum.rst
	MultiEval_ListPy_Modulation_Perror_Peak_StandardDev.rst