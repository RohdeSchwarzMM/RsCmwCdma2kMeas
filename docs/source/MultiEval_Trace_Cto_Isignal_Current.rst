Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CTO:ISIGnal:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cto_.Isignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: