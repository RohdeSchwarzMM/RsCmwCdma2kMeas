Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RPICh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RpiCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: