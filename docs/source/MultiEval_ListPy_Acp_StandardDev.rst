StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: