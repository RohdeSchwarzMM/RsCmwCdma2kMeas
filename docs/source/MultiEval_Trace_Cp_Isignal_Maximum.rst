Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:ISIGnal:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Isignal_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: