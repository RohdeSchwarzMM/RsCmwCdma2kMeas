Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Current.Current
	:members:
	:undoc-members:
	:noindex: