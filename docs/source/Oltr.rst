Oltr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CDMA:MEASurement<Instance>:OLTR
	single: ABORt:CDMA:MEASurement<Instance>:OLTR
	single: STOP:CDMA:MEASurement<Instance>:OLTR

.. code-block:: python

	INITiate:CDMA:MEASurement<Instance>:OLTR
	ABORt:CDMA:MEASurement<Instance>:OLTR
	STOP:CDMA:MEASurement<Instance>:OLTR



.. autoclass:: RsCmwCdma2kMeas.Implementations.Oltr.Oltr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_State.rst
	Oltr_Sequence.rst