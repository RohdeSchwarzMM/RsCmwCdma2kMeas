State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_State_All.rst