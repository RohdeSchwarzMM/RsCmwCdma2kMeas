Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: