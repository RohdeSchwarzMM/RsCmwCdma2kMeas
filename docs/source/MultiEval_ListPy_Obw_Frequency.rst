Frequency
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.obw.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Obw_Frequency_Current.rst
	MultiEval_ListPy_Obw_Frequency_Average.rst
	MultiEval_ListPy_Obw_Frequency_Maximum.rst
	MultiEval_ListPy_Obw_Frequency_StandardDev.rst
	MultiEval_ListPy_Obw_Frequency_Lower.rst
	MultiEval_ListPy_Obw_Frequency_Upper.rst