Obw
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Obw.Obw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Obw_Current.rst
	MultiEval_Obw_Average.rst
	MultiEval_Obw_Maximum.rst