State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CPO:QSIGnal:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cpo_.Qsignal_.State.State
	:members:
	:undoc-members:
	:noindex: