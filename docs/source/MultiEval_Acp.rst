Acp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:ACP
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:ACP
	FETCh:CDMA:MEASurement<Instance>:MEValuation:ACP
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:ACP



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Acp.Acp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Acp_Current.rst
	MultiEval_Acp_Average.rst
	MultiEval_Acp_Maximum.rst