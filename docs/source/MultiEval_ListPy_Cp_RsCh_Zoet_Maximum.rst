Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RSCH:ZOET:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.RsCh_.Zoet_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: