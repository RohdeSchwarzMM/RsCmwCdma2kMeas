State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RdcCh_.State.State
	:members:
	:undoc-members:
	:noindex: