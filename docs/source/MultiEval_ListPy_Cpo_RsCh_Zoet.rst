Zoet
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RsCh_.Zoet.Zoet
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cpo.rsCh.zoet.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cpo_RsCh_Zoet_State.rst
	MultiEval_ListPy_Cpo_RsCh_Zoet_Current.rst
	MultiEval_ListPy_Cpo_RsCh_Zoet_Average.rst
	MultiEval_ListPy_Cpo_RsCh_Zoet_Maximum.rst