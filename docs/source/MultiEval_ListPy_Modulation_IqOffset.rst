IqOffset
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqOffset.IqOffset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_IqOffset_Current.rst
	MultiEval_ListPy_Modulation_IqOffset_Average.rst
	MultiEval_ListPy_Modulation_IqOffset_Maximum.rst
	MultiEval_ListPy_Modulation_IqOffset_StandardDev.rst