Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.SingleCmw_.Connector.Connector
	:members:
	:undoc-members:
	:noindex: