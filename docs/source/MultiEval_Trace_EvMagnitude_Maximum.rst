Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: