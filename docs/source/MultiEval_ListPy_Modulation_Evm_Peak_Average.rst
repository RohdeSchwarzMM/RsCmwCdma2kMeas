Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Average.Average
	:members:
	:undoc-members:
	:noindex: