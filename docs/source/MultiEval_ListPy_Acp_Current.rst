Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Current.Current
	:members:
	:undoc-members:
	:noindex: