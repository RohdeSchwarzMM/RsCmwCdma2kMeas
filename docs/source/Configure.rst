Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:RCONfig
	single: CONFigure:CDMA:MEASurement<Instance>:DISPlay

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:RCONfig
	CONFigure:CDMA:MEASurement<Instance>:DISPlay



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings.rst
	Configure_MultiEval.rst
	Configure_Oltr.rst