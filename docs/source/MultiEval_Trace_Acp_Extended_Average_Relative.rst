Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:RELative



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Average_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: