Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: