Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:REACh:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.ReaCh_.Current.Current
	:members:
	:undoc-members:
	:noindex: