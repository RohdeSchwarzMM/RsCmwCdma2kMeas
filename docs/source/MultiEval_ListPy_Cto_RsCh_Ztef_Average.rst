Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RSCH:ZTEF:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RsCh_.Ztef_.Average.Average
	:members:
	:undoc-members:
	:noindex: