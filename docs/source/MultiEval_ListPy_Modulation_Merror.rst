Merror
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror.Merror
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Merror_Rms.rst
	MultiEval_ListPy_Modulation_Merror_Peak.rst