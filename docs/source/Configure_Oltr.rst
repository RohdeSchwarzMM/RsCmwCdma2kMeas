Oltr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:TOUT
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:REPetition
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:SEQuence
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:MOEXception

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:OLTR:TOUT
	CONFigure:CDMA:MEASurement<Instance>:OLTR:REPetition
	CONFigure:CDMA:MEASurement<Instance>:OLTR:SEQuence
	CONFigure:CDMA:MEASurement<Instance>:OLTR:MOEXception



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.Oltr.Oltr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.oltr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Oltr_Pstep.rst
	Configure_Oltr_RpInterval.rst
	Configure_Oltr_Ginterval.rst
	Configure_Oltr_Limit.rst