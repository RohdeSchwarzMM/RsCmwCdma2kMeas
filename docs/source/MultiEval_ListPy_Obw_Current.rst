Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Current.Current
	:members:
	:undoc-members:
	:noindex: