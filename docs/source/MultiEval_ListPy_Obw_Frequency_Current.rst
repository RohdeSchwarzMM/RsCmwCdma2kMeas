Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Current.Current
	:members:
	:undoc-members:
	:noindex: