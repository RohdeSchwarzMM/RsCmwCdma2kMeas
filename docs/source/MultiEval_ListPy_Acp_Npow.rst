Npow
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow.Npow
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.npow.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Npow_Current.rst
	MultiEval_ListPy_Acp_Npow_Average.rst
	MultiEval_ListPy_Acp_Npow_Maximum.rst
	MultiEval_ListPy_Acp_Npow_Minimum.rst
	MultiEval_ListPy_Acp_Npow_StandardDev.rst