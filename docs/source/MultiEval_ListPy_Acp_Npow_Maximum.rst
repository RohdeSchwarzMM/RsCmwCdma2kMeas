Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: