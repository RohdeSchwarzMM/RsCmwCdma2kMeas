Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: