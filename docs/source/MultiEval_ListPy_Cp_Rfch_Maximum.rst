Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Rfch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: