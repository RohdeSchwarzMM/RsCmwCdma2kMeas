Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage
	FETCh:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:OBW:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Obw_.Average.Average
	:members:
	:undoc-members:
	:noindex: