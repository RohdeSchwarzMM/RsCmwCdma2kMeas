EvMagnitude
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.EvMagnitude.EvMagnitude
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_EvMagnitude_Current.rst
	MultiEval_Trace_EvMagnitude_Average.rst
	MultiEval_Trace_EvMagnitude_Maximum.rst