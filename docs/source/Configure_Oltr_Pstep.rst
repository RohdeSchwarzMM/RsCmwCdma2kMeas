Pstep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:PSTep:DIRection
	single: CONFigure:CDMA:MEASurement<Instance>:OLTR:PSTep

.. code-block:: python

	CONFigure:CDMA:MEASurement<Instance>:OLTR:PSTep:DIRection
	CONFigure:CDMA:MEASurement<Instance>:OLTR:PSTep



.. autoclass:: RsCmwCdma2kMeas.Implementations.Configure_.Oltr_.Pstep.Pstep
	:members:
	:undoc-members:
	:noindex: