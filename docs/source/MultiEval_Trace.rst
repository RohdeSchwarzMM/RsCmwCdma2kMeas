Trace
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_EvMagnitude.rst
	MultiEval_Trace_Merror.rst
	MultiEval_Trace_Perror.rst
	MultiEval_Trace_Acp.rst
	MultiEval_Trace_Obw.rst
	MultiEval_Trace_Spectrum.rst
	MultiEval_Trace_Cdp.rst
	MultiEval_Trace_Cde.rst
	MultiEval_Trace_Cp.rst
	MultiEval_Trace_Cpo.rst
	MultiEval_Trace_Cto.rst
	MultiEval_Trace_Iq.rst