ReaCh
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.ReaCh.ReaCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.reaCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_ReaCh_State.rst
	MultiEval_ListPy_Cp_ReaCh_Current.rst
	MultiEval_ListPy_Cp_ReaCh_Average.rst
	MultiEval_ListPy_Cp_ReaCh_Maximum.rst
	MultiEval_ListPy_Cp_ReaCh_Minimum.rst