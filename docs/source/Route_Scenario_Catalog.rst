Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CDMA:MEASurement<Instance>:SCENario:CATalog:CSPath

.. code-block:: python

	ROUTe:CDMA:MEASurement<Instance>:SCENario:CATalog:CSPath



.. autoclass:: RsCmwCdma2kMeas.Implementations.Route_.Scenario_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: