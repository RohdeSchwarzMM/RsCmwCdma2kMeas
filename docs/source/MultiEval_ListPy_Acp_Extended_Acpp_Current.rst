Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpp_.Current.Current
	:members:
	:undoc-members:
	:noindex: