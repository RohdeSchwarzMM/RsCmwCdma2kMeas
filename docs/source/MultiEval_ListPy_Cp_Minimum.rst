Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MINimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MINimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MINimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:MINimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: