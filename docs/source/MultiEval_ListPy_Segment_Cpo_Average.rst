Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CPO:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Segment_.Cpo_.Average.Average
	:members:
	:undoc-members:
	:noindex: