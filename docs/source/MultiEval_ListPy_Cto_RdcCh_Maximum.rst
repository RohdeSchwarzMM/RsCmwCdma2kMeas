Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CTO:RDCCh:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.RdcCh_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: