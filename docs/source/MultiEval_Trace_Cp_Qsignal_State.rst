State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:CP:QSIGnal:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Cp_.Qsignal_.State.State
	:members:
	:undoc-members:
	:noindex: