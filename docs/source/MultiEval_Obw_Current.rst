Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:OBW:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Obw_.Current.Current
	:members:
	:undoc-members:
	:noindex: