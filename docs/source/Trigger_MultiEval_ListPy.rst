ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:CDMA:MEASurement<Instance>:MEValuation:LIST:MODE



.. autoclass:: RsCmwCdma2kMeas.Implementations.Trigger_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex: