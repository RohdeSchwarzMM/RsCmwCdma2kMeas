Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Average.Average
	:members:
	:undoc-members:
	:noindex: