Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:MAXimum

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RSCH:ZOET:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RsCh_.Zoet_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: