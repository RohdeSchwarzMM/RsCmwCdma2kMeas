Extended
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended.Extended
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.acp.extended.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Acp_Extended_Acpm.rst
	MultiEval_ListPy_Acp_Extended_Acpp.rst
	MultiEval_ListPy_Acp_Extended_Current.rst
	MultiEval_ListPy_Acp_Extended_Average.rst
	MultiEval_ListPy_Acp_Extended_Maximum.rst
	MultiEval_ListPy_Acp_Extended_Minimum.rst
	MultiEval_ListPy_Acp_Extended_StandardDev.rst