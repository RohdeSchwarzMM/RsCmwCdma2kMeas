Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Iq_.Current.Current
	:members:
	:undoc-members:
	:noindex: