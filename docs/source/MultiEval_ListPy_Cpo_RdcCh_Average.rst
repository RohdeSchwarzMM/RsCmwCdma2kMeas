Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RDCCh:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.RdcCh_.Average.Average
	:members:
	:undoc-members:
	:noindex: