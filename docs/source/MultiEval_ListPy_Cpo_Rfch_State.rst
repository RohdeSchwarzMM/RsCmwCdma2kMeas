State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:STATe

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CPO:RFCH:STATe



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cpo_.Rfch_.State.State
	:members:
	:undoc-members:
	:noindex: