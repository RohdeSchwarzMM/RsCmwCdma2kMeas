Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:RFCH:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Rfch_.Current.Current
	:members:
	:undoc-members:
	:noindex: