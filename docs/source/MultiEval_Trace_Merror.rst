Merror
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Merror.Merror
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Merror_Current.rst
	MultiEval_Trace_Merror_Average.rst
	MultiEval_Trace_Merror_Maximum.rst