StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: