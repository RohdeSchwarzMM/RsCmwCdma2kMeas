Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	FETCh:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Average_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: