Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:CURRent
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:CURRent

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:CURRent
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:CP:CURRent



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cp_.Current.Current
	:members:
	:undoc-members:
	:noindex: