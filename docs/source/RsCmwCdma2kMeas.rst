RsCmwCdma2kMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwCdma2kMeas('TCPIP::192.168.2.101::HISLIP')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwCdma2kMeas.RsCmwCdma2kMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Route.rst
	Configure.rst
	Trigger.rst
	MultiEval.rst
	Oltr.rst
	RpInterval.rst