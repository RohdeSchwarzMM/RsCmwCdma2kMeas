FreqError
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.FreqError.FreqError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_FreqError_Current.rst
	MultiEval_ListPy_Modulation_FreqError_Average.rst
	MultiEval_ListPy_Modulation_FreqError_Maximum.rst
	MultiEval_ListPy_Modulation_FreqError_StandardDev.rst