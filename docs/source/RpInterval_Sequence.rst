Sequence<Sequence>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.rpInterval.sequence.repcap_sequence_get()
	driver.rpInterval.sequence.repcap_sequence_set(repcap.Sequence.Nr1)





.. autoclass:: RsCmwCdma2kMeas.Implementations.RpInterval_.Sequence.Sequence
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rpInterval.sequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RpInterval_Sequence_Trace.rst