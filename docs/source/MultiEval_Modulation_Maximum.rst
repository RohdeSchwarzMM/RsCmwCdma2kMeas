Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum

.. code-block:: python

	READ:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	FETCh:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:MODulation:MAXimum



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: