Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	single: CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage

.. code-block:: python

	FETCh:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	CALCulate:CDMA:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage



.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Average.Average
	:members:
	:undoc-members:
	:noindex: