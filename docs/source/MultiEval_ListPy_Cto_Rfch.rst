Rfch
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Cto_.Rfch.Rfch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cto.rfch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cto_Rfch_State.rst
	MultiEval_ListPy_Cto_Rfch_Current.rst
	MultiEval_ListPy_Cto_Rfch_Average.rst
	MultiEval_ListPy_Cto_Rfch_Maximum.rst