Peak
----------------------------------------





.. autoclass:: RsCmwCdma2kMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak.Peak
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.merror.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Merror_Peak_Current.rst
	MultiEval_ListPy_Modulation_Merror_Peak_Average.rst
	MultiEval_ListPy_Modulation_Merror_Peak_Maximum.rst
	MultiEval_ListPy_Modulation_Merror_Peak_StandardDev.rst